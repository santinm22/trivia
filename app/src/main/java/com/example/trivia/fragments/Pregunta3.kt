package com.example.trivia.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation
import com.example.trivia.R

class Pregunta3 : Fragment() {
    lateinit var respuesta1: Button
    lateinit var respuesta2: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = inflater.inflate(R.layout.fragment_pregunta3, container, false)
        respuesta1 = binding.findViewById(R.id.btn_resp3_1)
        respuesta2 = binding.findViewById(R.id.btn_resp3_2)

        respuesta1.setOnClickListener { view ->
            Thread.sleep(2000)
            Navigation.findNavController(view).navigate(Pregunta3Directions.actionFragmentPregunta3ToFragmentRespuestaIncorrecta3(contador))
        }

        respuesta2.setOnClickListener { view ->
            contador++
            Thread.sleep(2000)
            Navigation.findNavController(view).navigate(Pregunta3Directions.actionFragmentPregunta3ToFragmentRespuestaCorrecta3(contador))

        }
        return binding
    }
}