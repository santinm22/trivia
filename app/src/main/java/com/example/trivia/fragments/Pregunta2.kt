package com.example.trivia.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation
import com.example.trivia.R


class Pregunta2 : Fragment() {
    lateinit var respuesta1: Button
    lateinit var respuesta2: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = inflater.inflate(R.layout.fragment_pregunta2, container, false)
        respuesta1 = binding.findViewById(R.id.btn1)
        respuesta2 = binding.findViewById(R.id.btn2)

        respuesta1.setOnClickListener { view ->
            contador++
            Thread.sleep(2000)
            Navigation.findNavController(view).navigate(Pregunta2Directions.actionFragmentPregunta2ToFragmentRespuestaCorrecta2(contador))

        }

        respuesta2.setOnClickListener { view ->
            Thread.sleep(2000)
            Navigation.findNavController(view).navigate(Pregunta2Directions.actionFragmentPregunta2ToFragmentRespuestaIncorecta2(contador))
        }
        return binding
    }


}