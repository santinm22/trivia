package com.example.trivia.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.navigation.Navigation
import com.example.trivia.R

class Respuesta_Correcta : Fragment() {
lateinit var siguiente_pregunta : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = inflater.inflate(R.layout.fragment_respuesta__correcta, container, false)
        siguiente_pregunta = binding.findViewById(R.id.btnSiguiente)
        siguiente_pregunta.setOnClickListener { view ->
            Navigation.findNavController(view).navigate(Respuesta_CorrectaDirections.actionFragmentRespuestaCorrectaToFragmentPregunta2(contador))
        }
        return binding
    }


}