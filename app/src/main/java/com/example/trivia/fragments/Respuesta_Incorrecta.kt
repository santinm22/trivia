package com.example.trivia.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation
import com.example.trivia.R


class Respuesta_Incorrecta : Fragment() {
    lateinit var siguiente_pregunta : Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = inflater.inflate(R.layout.fragment_respuesta__incorrecta, container, false)
        siguiente_pregunta = binding.findViewById(R.id.btnSiguiente2)
        siguiente_pregunta.setOnClickListener { view ->
            Navigation.findNavController(view).navigate(Respuesta_IncorrectaDirections.actionFragmentRespuestaIncorectaToFragmentPregunta2(contador))
        }
        return binding
    }
}