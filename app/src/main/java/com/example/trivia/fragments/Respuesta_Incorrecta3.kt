package com.example.trivia.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast
import androidx.navigation.Navigation
import com.example.trivia.R

class Respuesta_Incorrecta3 : Fragment() {
    lateinit var siguiente_pregunta : Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = inflater.inflate(R.layout.fragment_respuesta__incorrecta3, container, false)
        siguiente_pregunta = binding.findViewById(R.id.btnSiguiente4)
        siguiente_pregunta.setOnClickListener { view ->
            Navigation.findNavController(view).navigate(Respuesta_Incorrecta3Directions.actionFragmentRespuestaIncorrecta3ToFragmentMain2(contador))
        }
        arguments?.let {
            var args = Pregunta1Args.fromBundle(it)
            Toast.makeText(context,
                "Numero de preguntas correctas: ${args.preguntasCorrectas}",
                Toast.LENGTH_LONG).show()
        }
        val buttonShare: ImageButton = binding.findViewById(R.id.imageButton2)
        buttonShare.setOnClickListener{
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.setType("text/plain")
                .putExtra(Intent.EXTRA_TEXT, "He acertado $contador preguntas, Felicitame!!!")
            shareIntent
            startActivity(shareIntent)
        }

        return binding
    }
}