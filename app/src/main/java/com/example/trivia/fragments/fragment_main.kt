package com.example.trivia.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.example.trivia.R
var contador = 0
class fragment_main : Fragment() {
    lateinit var trivia: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = inflater.inflate(R.layout.fragment_main, container, false)
        trivia = binding.findViewById(R.id.btnJugar)

        trivia.setOnClickListener { view ->

            Navigation.findNavController(view).navigate(fragment_mainDirections.actionFragmentMain2ToFragmentPregunta1(0))
        }
        return binding
    }

}